package com.example.qingyun.basefunction;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import butterknife.ButterKnife;
import butterknife.OnClick;
import droidplugin.DroidPluginActivityTest;
import example.mvpmode.splash.SplashActivity;

import com.ashokvarma.bottomnavigation.BottomNavigationBar;
import com.ashokvarma.bottomnavigation.BottomNavigationItem;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import example.mvpmode.MVPLogin;
import bus.BusEventData;
import bus.MyBus;
import example.mono_tab.MonoActivity;

public class MainActivity extends AppCompatActivity{
    private Bus bus;
//    @BindView(R.id.result) TextView resultTextView;
      BottomNavigationBar bottomNavigationBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        bus= MyBus.getInstance();
        bus.register(this);

        bottomNavigationBar  = (BottomNavigationBar) findViewById(R.id.bottom_navigation_bar);

        bottomNavigationBar
                .addItem(new BottomNavigationItem(R.drawable.bottom_home, "Home"))
                .addItem(new BottomNavigationItem(R.drawable.bottom_discover, "Books"))
                .addItem(new BottomNavigationItem(R.drawable.bottom_search, "Music"))
                .addItem(new BottomNavigationItem(R.drawable.bottom_favorite, "favorite"))
                .addItem(new BottomNavigationItem(R.drawable.bottom_setting, "setting"))
                .initialise();

        bottomNavigationBar.setTabSelectedListener(new BottomNavigationBar.OnTabSelectedListener(){
            @Override
            public void onTabSelected(int position) {
            }
            @Override
            public void onTabUnselected(int position) {
            }
            @Override
            public void onTabReselected(int position) {
            }
        });

    }

    @OnClick({R.id.otto,R.id.pugin_test,R.id.button_splash})
    public void doOnClick(View view) {
        switch (view.getId()){
            case  R.id.otto:
                startActivity(new Intent(MainActivity.this, MonoActivity.class));
//                startActivity(new Intent(MainActivity.this, OttoActivity.class));
                break;
            case  R.id.pugin_test:
//                startActivity(new Intent(MainActivity.this, MVPLogin.class));
                startActivity(new Intent(MainActivity.this, DroidPluginActivityTest.class));
                break;
            case  R.id.button_splash:
                startActivity(new Intent(MainActivity.this, SplashActivity.class));
                break;
        }
    }


    @Subscribe
    public void setContent(BusEventData data){
        Snackbar.make(bottomNavigationBar,data.getMessage(),Snackbar.LENGTH_SHORT);
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        bus.unregister(this);
    }


}
