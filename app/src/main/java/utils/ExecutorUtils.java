package utils;

import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by sunsh on 2017/11/25.
 */

public class ExecutorUtils {
    public static ExecutorService executorService = Executors.newCachedThreadPool();

    public static void doOneWork(Runnable runnable){
        executorService.execute(runnable);
    }

}
