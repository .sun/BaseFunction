package utils;

import android.content.Context;
import android.util.Log;

import com.bumptech.glide.Glide;
import com.bumptech.glide.GlideBuilder;
import com.bumptech.glide.Registry;
import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.module.AppGlideModule;
import com.bumptech.glide.request.RequestOptions;
import com.example.qingyun.basefunction.R;

import java.io.InputStream;

import utils.OkHttpLoader;

/**
 * Created by sunsh on 2017/12/5.
 */
@GlideModule
public class MyAppModule extends AppGlideModule {
    @Override
    public void applyOptions(Context context, GlideBuilder builder) {
        int memoryCacheSizeBytes=10*1024*1024;
//        builder.setMemoryCache(new M);
        builder.setLogLevel(Log.DEBUG);
    }
    @Override
    public void registerComponents(Context context, Glide glide, Registry registry) {

        registry.replace(GlideUrl.class, InputStream.class,new OkHttpLoader.Factory());
    }


}
