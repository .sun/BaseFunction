package utils;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import timber.log.Timber;

/**
 * Created by sunsh on 2017/12/2.
 */

public class FileLoggingTree extends Timber.Tree {

   private String CacheDiaPath="";
    public FileLoggingTree(Context context) {
        CacheDiaPath= context.getCacheDir().toString();
    }

    @Override
    protected void log(int priority, String tag, String message, Throwable t) {
//
        if (TextUtils.isEmpty(CacheDiaPath)) {
            return;
        }
        File file = new File(CacheDiaPath + "/log.txt");
        Log.v("dyp", "file.path:" + file.getAbsolutePath() + ",message:" + message);
        FileWriter writer = null;
        BufferedWriter bufferedWriter = null;
        try {
            writer = new FileWriter(file);
            bufferedWriter = new BufferedWriter(writer);
            bufferedWriter.write(message);
            bufferedWriter.flush();

        } catch (IOException e) {
            Log.v("dyp", "存储文件失败");
            e.printStackTrace();
        } finally {
            if (bufferedWriter != null) {
                try {
                    bufferedWriter.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
