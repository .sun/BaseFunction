package utils;

import android.app.Application;
import android.content.Context;



import com.morgoo.droidplugin.PluginHelper;
import com.morgoo.droidplugin.pm.PluginManager;


/**
 * Created by sunsh on 2017/11/8.
 */

public class SunApplication extends Application {

    public SunApplication() {
    }

    /**
     * penaltyLog  logcat 中打印日志
     */
    @Override
    public void onCreate() {
        super.onCreate();

//        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().detectAll().penaltyLog().build());
//        StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder().detectAll().penaltyLog().build());
//        BlockCanary.install(this,new BlockCanaryContext()).start();
        //
//        DatabaseUtil.createDatabase(this);

//        // 日志处理
//        if (BuildConfig.DEBUG){
//            Timber.plant(new Timber.DebugTree());
//        }else {
//            Timber.plant(new FileLoggingTree(this));
////            Timber.plant(new CrashReportingTree());
//        }
        // add 2017 12 27  插件化框架初始化
        PluginHelper.getInstance().applicationOnCreate(getBaseContext());


    }

    @Override
    protected void attachBaseContext(Context base) {
        PluginHelper.getInstance().applicationAttachBaseContext(base);
        super.attachBaseContext(base);

    }
}
