package mode;

/**
 * Created by sunsh on 2017/11/13.
 */

public class BuliderUser {
    private final String mNickName;
    private final String mRealName;
    private final String mGender;
    private final int mAge;
    private final String mPhone;

    private BuliderUser(Builder builder) {
        mNickName = builder.mNickName;
        mRealName=builder.mRealName;
        mGender = builder.mGender;
        mAge = builder.mAge;
        mPhone = builder.mPhone;
    }


    public String getmNickName() {
        return mNickName;
    }

    public String getmRealName() {
        return mRealName;
    }

    public String getmGender() {
        return mGender;
    }

    public int getmAge() {
        return mAge;
    }

    public String getmPhone() {
        return mPhone;
    }

    public static final class Builder {
        private String mNickName;
        private  String mRealName;
        private String mGender;
        private int mAge;
        private String mPhone;

        public Builder() {
        }


        public Builder mRealName(String realName){
            mRealName=realName;
            return this;
        }

        public Builder mNickName(String val) {
            mNickName = val;
            return this;
        }

        public Builder mGender(String val) {
            mGender = val;
            return this;
        }

        public Builder mAge(int val) {
            mAge = val;
            return this;
        }

        public Builder mPhone(String val) {
            mPhone = val;
            return this;
        }

        public BuliderUser build() {
            return new BuliderUser(this);
        }
    }

    // for test
    public  static void test(){
       new  Builder()
               .mRealName("sunshenzhen")
               .mNickName("sunqingyun")
               .build();

    }

}
