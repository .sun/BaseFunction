package bus;

import com.squareup.otto.Bus;

/**
 * Created by sunsh on 2017/11/4.
 */

public class MyBus  extends Bus{
    private volatile static  MyBus bus;//当一个共享变量被volatile修饰时，它会保证修改的值会立即被更新到主存，当有其他线程需要读取时，它会去内存中读取新值

    private MyBus() {
    }
    public static MyBus getInstance(){
        if (bus==null){
            synchronized (MyBus.class){
                if (bus==null){
                    bus=new MyBus();
                }
            }
        }
        return bus;
    }
}
