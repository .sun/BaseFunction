package bus;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.qingyun.basefunction.R;

import net.tsz.afinal.FinalActivity;
import net.tsz.afinal.FinalHttp;
import net.tsz.afinal.annotation.view.ViewInject;
import net.tsz.afinal.http.AjaxCallBack;

import bus.BusEventData;
import bus.MyBus;

public class OttoActivity extends FinalActivity {
    @ViewInject(id= R.id.button) Button button;
    @ViewInject(id=R.id.show_result) TextView showResult;
    @ViewInject(id =R.id.afinalHttp)Button fhButton;
    FinalHttp finalHttp;
    String url="https://www.zhihu.com/";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom);
        finalHttp=new FinalHttp();

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyBus.getInstance().post(new BusEventData("孙青云的Otto"));
                finish();
            }
        });

    }




    protected  void  testHttp(View v){
        finalHttp.get(url, new AjaxCallBack<String>() {
            @Override
            public void onStart() {
                super.onStart();
            }

            @Override
            public void onLoading(long count, long current) {
               //请求响应过程中会执行此方法，每隔1秒自动回调一次
                super.onLoading(count, current);
                showResult.setText(current + "/" + count);
            }

            @Override
            public void onSuccess(String s) {
                super.onSuccess(s);
                showResult.setText(s);

            }

            @Override
            public void onFailure(Throwable t, int errorNo, String strMsg) {
                super.onFailure(t, errorNo, strMsg);
            }
        });
       // http://www.oschina.net/p/afinal?fromerr=HYI1eZ2F
    }


    /***
     * post
     *      AjaxParams params = new AjaxParams();
            params.put("username", "testname");
            params.put("password", "123456");
            params.put("profile_picture", new File("/mnt/sdcard/test.jpg")); // 上传文件
            params.put("profile_picture2", inputStream); // 上传数据流
            params.put("profile_picture3", new ByteArrayInputStream(bytes)); // 提交字节流
            fh.post("http://www.test.com", params, new AjaxCallBack(){......});
     *
     *
     *    FinalHttp fh = new FinalHttp();  
         //调用download方法开始下载
         HttpHandler handler = fh.download("http://www.xxx.com/下载路径/xxx.apk", //这里是下载的路径
         true,//true:断点续传 false:不断点续传（全新下载）
         "/mnt/sdcard/testapk.apk", //这是保存到本地的路径
         new AjaxCallBack() {  
         @Override  
         public void onLoading(long count, long current) {  
            textView.setText("下载进度："+current+"/"+count);  
         }  

         @Override  
         public void onSuccess(File t) {  
         textView.setText(t==null?"null":t.getAbsoluteFile().toString());  
         }  

     });  
         //调用stop()方法停止下载
          handler.stop();

     */

}
