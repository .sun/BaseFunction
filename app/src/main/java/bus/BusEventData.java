package bus;

/**
 * Created by sunsh on 2017/11/4.
 */

public class BusEventData {
      public String message;

    public BusEventData(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
