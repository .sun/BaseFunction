package example.mvpmode.presenter;


import android.os.Handler;

import example.mvpmode.model.LoginInterface;
import example.mvpmode.model.OnLoginListener;
import example.mvpmode.model.Teacher;
import example.mvpmode.model.UserLogin;
import example.mvpmode.view.IUserLoginView;

/**
 * Created by sunsh on 2017/12/1.
 *
 * resenter是用作Model和View之间交互的桥梁，那么应该有什么方法呢？

 其实也是主要看该功能有什么操作，比如本例，两个操作:login和clear。
 */

public class UserLoginPresenter {

    private LoginInterface userInterface;
    IUserLoginView userLoginView;
    private Handler mhandler= new Handler();

    public UserLoginPresenter(IUserLoginView userLoginView) {
        this.userLoginView = userLoginView;
        userInterface=new UserLogin();
    }

    public void login(){
        userLoginView.showLoading();

        userInterface.login(userLoginView.getPassWord(), userLoginView.getUserName(), new OnLoginListener() {
            @Override
            public void loginSuccess(final Teacher teacher) {
                mhandler.post(new Runnable() {
                    @Override
                    public void run() {
                        userLoginView.toMainActivity(teacher);
                        userLoginView.hideLoading();
                    }
                });
            }

            @Override
            public void loginFailed() {
             mhandler.post(new Runnable() {
                 @Override
                 public void run() {
                     userLoginView.showFailedError();
                     userLoginView.hideLoading();
                 }
             });
            }
        });
    }

    public void clear(){
     userLoginView.clearPassword();
     userLoginView.clearPassword();
    }
}
