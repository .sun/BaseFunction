package example.mvpmode;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.qingyun.basefunction.R;

import net.tsz.afinal.FinalActivity;
import net.tsz.afinal.annotation.view.ViewInject;

import example.mvpmode.model.Teacher;
import example.mvpmode.presenter.UserLoginPresenter;
import example.mvpmode.view.IUserLoginView;

/***
 * 如何针对一个Activity 编写 MVP 代码
 *
 *    View 对应于Activity，负责View的绘制以及与用户交互
      Model 依然是业务逻辑和实体模型
      Presenter 负责完成View于Model间的交互
    将复杂的逻辑代码提取到了Presenter中进行处理。
 对应的好处就是，耦合度更低，更方便的进行测试。
 */

/***
 * 我们的View的实现类，
 * 哈，其实就是Activity，文章开始就说过，
 * MVP中的View其实就是Activity。
 */



public class MVPLogin extends FinalActivity implements IUserLoginView {
    @ViewInject(id =R.id.editText) EditText mEtUsername;
    @ViewInject(id =R.id.editText2) EditText mEtPassword;
    @ViewInject(id =R.id.button3) Button mBtnLogin;
    @ViewInject(id =R.id.button4) Button mBtnClear;
    @ViewInject(id =R.id.progressBar) ProgressBar mPbLoading;

    private UserLoginPresenter mUserLoginPresenter=new UserLoginPresenter(this);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_mvplogin);
        initView();

    }

    private void initView() {
        mBtnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 mUserLoginPresenter.login();
            }
        });
        mBtnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mUserLoginPresenter.clear();
            }
        });

    }

    @Override
    public String getUserName() {
        return mEtUsername.getText().toString();
    }

    @Override
    public String getPassWord() {
        return mEtPassword.getText().toString();
    }

    @Override
    public void clearUserName() {
        mEtUsername.setText("");
    }

    @Override
    public void clearPassword() {
        mEtPassword.setText("");
    }

    @Override
    public void showLoading() {
        mPbLoading.setVisibility(View.VISIBLE);

    }

    @Override
    public void hideLoading() {
        mPbLoading.setVisibility(View.GONE);
    }

    @Override
    public void toMainActivity(Teacher user) {
        Toast.makeText(this, user.getUsername() +
                " login success , to MainActivity", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showFailedError() {
        Toast.makeText(this,
                "login failed", Toast.LENGTH_SHORT).show();
    }


}
