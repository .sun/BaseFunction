package example.mvpmode.model;

/**
 * Created by sunsh on 2017/12/1.
 */

public class UserLogin implements LoginInterface {

    @Override
    public void login(final String userName, final String password, final OnLoginListener loginListener) {
        new Thread(){
            @Override
            public void run() {
                super.run();
                 try {
                     Thread.sleep(1000);

                 }catch (InterruptedException e){
                     e.printStackTrace();
                 }

                 if ("sun".equals(userName)&&"123".equals(password)){
                     loginListener.loginSuccess(new Teacher(userName,password));

                 }else {
                     loginListener.loginFailed();
                 }

            }
        }.start();

    }
}
