package example.mvpmode.model;

/**
 * Created by sunsh on 2017/12/1.
 */

public interface OnLoginListener  {

    void loginSuccess(Teacher teacher);
    void loginFailed();
}
