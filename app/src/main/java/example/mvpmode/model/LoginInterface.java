package example.mvpmode.model;

/**
 * Created by sunsh on 2017/12/1.
 */

public interface LoginInterface {

    public void login(String userName,String password, OnLoginListener loginListener);

}
