package example.Rxjava;

import android.util.Log;

import java.util.Observable;

import rx.Scheduler;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by sunsh on 2017/12/25.
 */

public class RxJavaTest {
    public void  testGetResult(){
        rx.Observable.just("sun")
                .subscribe(new Action1<String>() {
                    @Override
                    public void call(String s) {
                     Log.d("RxJava",s);
                    }
                });
        // map 将已发送的消息转换成另一种形式
        rx.Observable.just("sun").map(new Func1<String, String>() {
            @Override
            public String call(String s) {

                return s+" @";
            }
        });

        rx.Observable.just(getResult())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<Boolean>() {
                    @Override
                    public void call(Boolean aBoolean) {
                        if (aBoolean){

                        }
                    }
                });


    }




    public boolean getResult(){
        return true;
    }
}
