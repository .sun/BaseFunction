package example.other;

import android.util.Log;

import net.tsz.afinal.annotation.sqlite.Id;
import net.tsz.afinal.annotation.sqlite.Table;

import java.io.Serializable;

/**
 * Created by sunsh on 2017/11/8.
 */
@Table(name= "people")
public class People implements Serializable,Cloneable {
    @Id(column = "id")
    private int id;

    private int sexy;
    private String name;

    private String info;

    public People(int sexy, String name, String info) {
        this.sexy = sexy;
        this.name = name;
        this.info = info;
    }

    /**
     * 克隆对象
     *
     * @return
     */
    @Override
    public Object clone() {
        Object o = null;
        try {
            o = (People) super.clone();
        } catch (CloneNotSupportedException e) {
            System.out.println(e.toString());
            Log.i("clone exception", e.toString());
        }
        return o;
    }
    @Override
    public String toString() {
        return "People{" +
                "id=" + id +
                ", sexy=" + sexy +
                ", name='" + name + '\'' +
                ", info='" + info + '\'' +
                '}';
    }
}
