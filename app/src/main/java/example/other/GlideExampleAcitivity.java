package example.other;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.example.qingyun.basefunction.R;


import net.tsz.afinal.FinalActivity;

import java.util.ArrayList;
import java.util.List;

import utils.MyAppModule;

/****
 * 至于为什么要在应用包名下面，源码里面可以看出来。
 * 特别是源码注释一定要看，亲情提醒。
 *
 *
 File file = new File(getExternalCacheDir() + "/image.jpg");
 Glide.with(this).load(file).into(imageView);

 // 加载应用资源
 int resource = R.drawable.image;
 Glide.with(this).load(resource).into(imageView);

 // 加载二进制流
 byte[] image = getImageBytes();
 Glide.with(this).load(image).into(imageView);

 // 加载Uri对象
 Uri imageUri = getImageUri();
 Glide.with(this).load(imageUri).into(imageView);
 */

public class GlideExampleAcitivity extends FinalActivity {

    GridView gridView;
    PicAdapter picAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flash);
        gridView=findViewById(R.id.grid_view);
        List<String> urls=new ArrayList<>();
        for (int i=0;i<10;i++){
            String url="http://47.93.96.47:8080/PictureShowWeb/pic/a.jpg";
            urls.add(url);
        }
        picAdapter=new PicAdapter(this,urls);
        gridView.setAdapter(picAdapter);

    }


    class PicAdapter extends BaseAdapter{
        private Context context;
        private List<String> urls=new ArrayList<>();
        public PicAdapter() {
        }

        public PicAdapter(Context context, List<String> urls) {
            this.context = context;
            this.urls = urls;
        }




        public void setUrls(List<String> urls) {
            this.urls = urls;
        }

        @Override
        public int getCount() {
            return urls.size();
        }

        @Override
        public Object getItem(int i) {
            return urls.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {

            ViewHolder viewHolder=null;
            if (view==null){
                viewHolder=new ViewHolder();
                view= LayoutInflater.from(context).inflate(R.layout.item_image,null);
                viewHolder.imageView=view.findViewById(R.id.image);
                view.setTag(viewHolder);
            }else {
                viewHolder= (ViewHolder) view.getTag();
            }
            if (urls!=null&&urls.size()>0){
                RequestOptions requestOptions=new RequestOptions()
                        .placeholder(R.drawable.loading)
                        .diskCacheStrategy(DiskCacheStrategy.ALL);
                Glide.with(context).load(urls.get(i))
                        .apply(requestOptions)
                        .into(viewHolder.imageView);
            }
            return view;
        }

        class ViewHolder {
            ImageView imageView;
        }
    }

}
