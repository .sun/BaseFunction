package example.mono_tab;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * Created by sunsh on 2017/11/24.
 */

public class FixedTabsPagerAdapter extends FragmentPagerAdapter {
    public FixedTabsPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new Conversation();
            case 1:
                return new Conversation();
            case 2:
                return new Conversation();
            default:
                return null;
        }


    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override


    public CharSequence getPageTitle(int position) {
        switch (position){
            case 0:
                return "sun";
            case 1:
                return "qing";
            case 2:
                return "yun";
            default:
                return null;
        }

    }
}
