package example.mvpmode.view;

/**
 * Created by sunsh on 2017/12/22.
 */

public interface HideFabView {

    void hideOrShowFAB(boolean hide);
}
