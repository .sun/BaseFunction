package example.mvpmode.home;

import android.content.Intent;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;

import com.ashokvarma.bottomnavigation.BottomNavigationBar;
import com.ashokvarma.bottomnavigation.BottomNavigationItem;
import com.example.baselibrary.activtity.AppActivity;
import com.example.baselibrary.activtity.BaseFragment;
import com.github.rubensousa.floatingtoolbar.FloatingToolbar;

import example.mvpmode.R;
import example.mvpmode.view.HideFabView;


public class HomeActivity extends AppActivity implements FloatingToolbar.ItemClickListener,HideFabView{
    private Toolbar mToolbar;
    private long exitTime=0;

    private FloatingActionButton mFab;
    private FloatingToolbar mBottomBar;
    private int mCurrentPos;//当前fragment

    @Override
    protected BaseFragment getFirstFragment() {
        return GirlsFragment.getInstance();
    }

    @Override
    protected int getContentViewId() {
        return R.layout.activity_home;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initView();
    }

    protected void initView() {
        mToolbar=findViewById(R.id.toolbar);
        mToolbar.setTitle(R.string.app_name);
        setSupportActionBar(mToolbar);

        mFab = (FloatingActionButton) findViewById(R.id.fab);
        mBottomBar = (FloatingToolbar) findViewById(R.id.floatingToolbar);
        mBottomBar.attachFab(mFab);
        mBottomBar.setClickListener(this);
    }

    @Override
    protected int getFragmentContentId() {
        return R.id.girls_fragment;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        int id =item.getItemId();
        if (id==R.id.action_about){
//            startActivity(new Intent(HomeActivity.this,AboutActivity.class));
        }else if (id==R.id.action_add){

        }else if (id == R.id.action_change_tag){

        }
        return true;
    }


    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if(mCurrentPos ==1){
            menu.setGroupVisible(R.id.menu_discover,true);
        }else{
            menu.setGroupVisible(R.id.menu_discover,false);
        }

        if(mCurrentPos ==2){
            menu.setGroupVisible(R.id.menu_favorite,true);
        }else{
            menu.setGroupVisible(R.id.menu_favorite,false);
        }

        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode ==KeyEvent.KEYCODE_BACK &&event.getAction() ==KeyEvent.ACTION_DOWN){
            if ((System.currentTimeMillis() -exitTime>2000)){
                Snackbar.make(mToolbar,"再按一次退出呀~",Snackbar.LENGTH_LONG).show();
                exitTime =System.currentTimeMillis();
            }else {
                finish();
                System.exit(0);
            }
            return true;
        }
        return super.onKeyDown(keyCode,event);
    }

    @Override
    public void onItemClick(MenuItem item) {


    }

    @Override
    public void onItemLongClick(MenuItem item) {

    }

    @Override
    public void hideOrShowFAB(boolean hide) {
        if(hide){
            if(mBottomBar.isShowing()) {
                mBottomBar.hide();
            }

            if(Build.VERSION.SDK_INT>19) mFab.hide();
        }else{
            if(mBottomBar.isShowing())return;
            mFab.show();
        }
    }


    //当前的题目
    private void setCurrentTitle(int pos) {
        if(pos==0){
            mToolbar.setTitle("杂图集");
        }else if(pos==2){
            mToolbar.setTitle("收藏");
        }else if(pos==3){
            mToolbar.setTitle("偏好设置");
        }
    }

}
