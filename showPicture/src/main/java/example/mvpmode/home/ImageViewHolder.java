package example.mvpmode.home;

import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestOptions;
import com.jude.easyrecyclerview.adapter.BaseViewHolder;

import example.mvpmode.R;
import example.mvpmode.data.PictureData;
import example.mvpmode.util.GlideConfiguration;
import example.mvpmode.util.ImageHelper;

/**
 * Created by sunsh on 2017/12/20.
 */

public class ImageViewHolder extends BaseViewHolder<PictureData> {
    private ImageView imgPicture;
    public ImageViewHolder(ViewGroup parent) {
        super(parent, R.layout.item_girl);
//        imgPicture= (ImageView) itemView;
        imgPicture = $(R.id.girl_img);
    }

    @Override
    public void setData(PictureData data) {
        super.setData(data);


//        ViewGroup.LayoutParams params = imgPicture.getLayoutParams();
//        DisplayMetrics dm = getContext().getResources().getDisplayMetrics();
//        int width = dm.widthPixels/2;//宽度为屏幕宽度一半
//        int height = data.getHeight()*width/data.getWidth();//计算View的高度
//
//        params.height = height;
//        imgPicture.setLayoutParams(params);

        RequestOptions requestOptions=new RequestOptions()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .placeholder(ImageHelper.getRandomColor());
        Glide.with(getContext())
                .load(data.getUrl())
                .apply(requestOptions)

                .into(imgPicture);
    }
}
