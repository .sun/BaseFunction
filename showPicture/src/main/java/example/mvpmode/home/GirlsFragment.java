package example.mvpmode.home;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.DisplayMetrics;
import android.view.View;

import com.example.baselibrary.activtity.BaseFragment;
import com.jude.easyrecyclerview.EasyRecyclerView;
import com.jude.easyrecyclerview.adapter.BaseViewHolder;
import com.jude.easyrecyclerview.adapter.RecyclerArrayAdapter;
import com.jude.easyrecyclerview.decoration.SpaceDecoration;

import java.util.ArrayList;

import example.mvpmode.R;
import example.mvpmode.data.DataProvider;
import example.mvpmode.data.PictureData;
import example.mvpmode.detail.ImageDetailActivity;
import example.mvpmode.view.HideFabView;

/**
 * Created by sunsh on 2017/12/20.
 *
 * 瀑布流的实现
 *
 *
 * http://www.jianshu.com/p/34b78dae9f57
 * 和RecyclerView关联起来，在滚动的时候隐藏FloatingToolbar：
 *
 mFloatingToolbar.attachRecyclerView(recyclerView);
 (可选) 使用 show() 和 hide() 在任何时候触发 transition:
 mFloatingToolbar.show();
 mFLoatingToolbar.hide();
 */

public class GirlsFragment extends BaseFragment implements PresentView {
  private EasyRecyclerView mEasyRecyclerView;
    private ImageAdapter mAdapter;

    private int page = 1;
    private int size = 20;
    private ArrayList<PictureData> data;
    private ImagePresenter presenter;
    protected HideFabView mHideFabView;
    protected boolean isHideFab = false;

    public static GirlsFragment getInstance() {
        GirlsFragment mainFragment = new GirlsFragment();
        return mainFragment;
    }


    @Override
    protected int getLayoutId() {
        return R.layout.fragment_home;
    }

    @Override
    protected void initView(View view, Bundle savedInstanceState) {
        mEasyRecyclerView=view.findViewById(R.id.girls_recycler_view);
        StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        mEasyRecyclerView.setLayoutManager(staggeredGridLayoutManager);
        mAdapter = new ImageAdapter(getContext());
        mEasyRecyclerView.setAdapter(mAdapter);
        data = new ArrayList<>();
        presenter=new ImagePresenter(this);

        //添加边框
        SpaceDecoration itemDecoration = new SpaceDecoration((int) convertDpToPixel(8,getContext()));
        itemDecoration.setPaddingEdgeSide(true);
        itemDecoration.setPaddingStart(true);
        itemDecoration.setPaddingHeaderFooter(false);
        mEasyRecyclerView.addItemDecoration(itemDecoration);

        mAdapter.setMore(R.layout.load_more_layout, new RecyclerArrayAdapter.OnMoreListener() {
                    @Override
                    public void onMoreShow() {
                        if (data.size() % 20 == 0) {
                            page++;
                            presenter.addData(page);
                        }

                    }

                    @Override
                    public void onMoreClick() {

                    }
            });

        mAdapter.setNoMore(R.layout.no_more_layout);
        mAdapter.setError(R.layout.error_layout);


        mEasyRecyclerView.setRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mEasyRecyclerView.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                         data.clear();

                         mAdapter.clear();
                        data.addAll(DataProvider.getPictures(0));
                         mAdapter.addAll(DataProvider.getPictures(0));
                    }
                },1000);
            }
        });


        mAdapter.setOnMyItemClickListener(new ImageAdapter.OnMyItemClickListener() {
            @Override
            public void onItemClick(int position, BaseViewHolder holder) {
                Intent intent = new Intent(mBaseActivity, ImageDetailActivity.class);
                intent.putExtra("girls", data);
                intent.putExtra("current", position);
                ActivityOptionsCompat options = ActivityOptionsCompat.makeScaleUpAnimation(holder.itemView, holder.itemView.getWidth() / 2, holder.itemView.getHeight() / 2, 0, 0);
                startActivity(intent, options.toBundle());
            }
        });


        addPresentViewData(page);

        mEasyRecyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy>0){
                    onScrollDown(recyclerView,dx,dy);
                }else{
                    onScrollUp(recyclerView,dx,dy);
                }
            }
        });

        //
        setHomeFabHideEnable((HideFabView) mBaseActivity,true);
    }
    /**是否滑动时隐藏fab*/
    protected void setHomeFabHideEnable(HideFabView hideFabView, boolean isHideFavb){
        this.mHideFabView = hideFabView;
        this.isHideFab = isHideFavb;
    }

    protected void onScrollUp(RecyclerView recyclerView, int dx, int dy){
        if(isHideFab && mHideFabView!=null) mHideFabView.hideOrShowFAB(false);
    }

    protected void onScrollDown(RecyclerView recyclerView, int dx, int dy) {
        if(isHideFab && mHideFabView!=null) mHideFabView.hideOrShowFAB(true);
    }

    @Override
    public void addPresentViewData(final int currentPage) {
        mEasyRecyclerView.postDelayed(new Runnable() {
            @Override
            public void run() {
                data.addAll(DataProvider.getImage(currentPage));
                mAdapter.addAll(DataProvider.getImage(currentPage));
            }
        },1000);

    }

    public static float convertDpToPixel(float dp, Context context){
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return px;
    }


}
