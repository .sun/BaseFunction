package example.mvpmode.home;

/**
 * Created by sunsh on 2017/12/20.
 */

public class ImagePresenter {
    private PresentView presentView;

    public ImagePresenter(PresentView presentView) {
        this.presentView = presentView;
    }

    public void addData(int page){
        presentView.addPresentViewData(page);
    }



}
