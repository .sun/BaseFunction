package example.mvpmode.home;

/**
 * Created by sunsh on 2017/12/20.
 */

public interface PresentView {
    void addPresentViewData(int page);
}
