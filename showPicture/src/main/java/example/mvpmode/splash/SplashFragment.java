package example.mvpmode.splash;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.transition.BitmapTransitionFactory;
import com.example.baselibrary.activtity.ActivityManager;
import com.example.baselibrary.activtity.BaseFragment;


import butterknife.BindView;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import example.mvpmode.R;
import example.mvpmode.R2;
import example.mvpmode.home.HomeActivity;
import example.mvpmode.util.GlideConfiguration;

/**
 * Created by sunsh on 2017/12/18.
 */

public class SplashFragment extends BaseFragment   {

//    @BindView(R2.id.splash_image)
    ImageView mSplashImg;

//    private Unbinder unbinder;


    public static SplashFragment getInstance(){
        return new SplashFragment();
    }
    @Override
    protected int getLayoutId() {
        return R.layout.fragment_spash;
    }

    @Override
    protected void initView(View view, Bundle savedInstanceState) {
 // 空指针异常
//        unbinder =  ButterKnife.bind(this,view);
        mSplashImg=view.findViewById(R.id.splash_image);
    }



    @Override
    public void onResume() {
        super.onResume();
        showImage();
    }

    public void showImage() {
        RequestOptions requestOptions=new RequestOptions()
//                .placeholder(R.drawable.loading)
                .diskCacheStrategy(DiskCacheStrategy.ALL);

        Glide.with(getActivity())
                .load(R.drawable.spash)
                .apply(requestOptions)
                .transition(new DrawableTransitionOptions().crossFade(1000))
                .into(mSplashImg);

        Log.i("showImage","handler");
        Handler handler=new Handler ();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(getActivity(), HomeActivity.class));
                ActivityManager.getInstance().finishActivity();
            }
        },1000);


    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
//        unbinder.unbind();
    }
}
