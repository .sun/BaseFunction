package example.mvpmode.splash;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.example.baselibrary.activtity.AppActivity;
import com.example.baselibrary.activtity.BaseActivity;
import com.example.baselibrary.activtity.BaseFragment;

import example.mvpmode.R;

public class SplashActivity extends AppActivity {
    @Override
    protected BaseFragment getFirstFragment() {
        return SplashFragment.getInstance();
    }
    @Override
    protected int getContentViewId() {
        return R.layout.activity_splash;
    }
    @Override
    protected int getFragmentContentId() {
        return R.id.splash_fragment;
    }

}
