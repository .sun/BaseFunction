package example.mvpmode.util;

import java.util.Random;

import example.mvpmode.R;

/**
 * Created by sunsh on 2017/12/21.
 */

public class ImageHelper {
    private static int[] defaultColor=new int[]{R.color.DefaultGreen,R.color.DefaultBlue,
            R.color.DefaultRed, R.color.DefaultPurple};
    public static int getRandomColor() {
        Random random=new Random();
        return defaultColor[ random.nextInt(4)];
    }

}
