package example.mvpmode.data;

import java.io.Serializable;

/**
 * Created by sunsh on 2017/12/20.
 */

public class PictureData implements Serializable{
    int width;
    int height;

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public PictureData(int width, int height, String url) {
        this.width = width;
        this.height = height;
        this.url = url;
    }

    private String url;

    public PictureData(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
