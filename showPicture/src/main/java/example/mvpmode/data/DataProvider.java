package example.mvpmode.data;

import java.net.URI;
import java.util.ArrayList;
import java.util.Objects;

/**
 * Created by sunsh on 2017/12/20.
 */

public  class DataProvider {
    private static String URL="http://47.93.96.47:8080/PictureShowWeb/pic/huaban/";
    public static String imageUrl (int str){
        return URL+str+".jpg";
    }



    static final PictureData[] VIRTUAL_PICTURE = {
            new PictureData(566,800,"http://o84n5syhk.bkt.clouddn.com/57154327_p0.png"),
            new PictureData(2126,1181,"http://o84n5syhk.bkt.clouddn.com/57180221_p0.jpg"),
            new PictureData(1142,800,"http://o84n5syhk.bkt.clouddn.com/57174070_p0.jpg"),
            new PictureData(550,778,"http://o84n5syhk.bkt.clouddn.com/57166531_p0.jpg"),
            new PictureData(1085,755,"http://o84n5syhk.bkt.clouddn.com/57151022_p0.jpg"),
            new PictureData(656,550,"http://o84n5syhk.bkt.clouddn.com/57172236_p0.jpg"),
            new PictureData(1920,938,"http://o84n5syhk.bkt.clouddn.com/57174564_p0.jpg"),
            new PictureData(1024,683,"http://o84n5syhk.bkt.clouddn.com/57156832_p0.jpg"),
            new PictureData(723,1000,"http://o84n5syhk.bkt.clouddn.com/57151474_p0.png"),
            new PictureData(2000,1667,"http://o84n5syhk.bkt.clouddn.com/57156623_p0.png"),
    };
    public static ArrayList<PictureData> getPictures(int page){
        ArrayList<PictureData> arrayList = new ArrayList<>();
        for (int i = 0; i < VIRTUAL_PICTURE.length; i++) {
            arrayList.add(VIRTUAL_PICTURE[i]);
        }
        return arrayList;
    }

    public static ArrayList<PictureData> getImage(int page){
        ArrayList<PictureData> arrayList = new ArrayList<>();
        for (int i=1+(page*20);i<=(page+1)*20;i++){
            arrayList.add(new PictureData(imageUrl(i)));
        }
        return arrayList;
    }


}
