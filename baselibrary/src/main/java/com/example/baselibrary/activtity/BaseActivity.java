package com.example.baselibrary.activtity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;


/**
 * Created by QingYun on 2017/10/23.
 */

public abstract class BaseActivity extends AppCompatActivity implements View.OnClickListener {
    public static final String TAG = BaseActivity.class.getSimpleName();

    // ===========================================================
    // Constants
    // ===========================================================

    // ===========================================================
    // Fields
    // ===========================================================

    // ===========================================================
    // Constructors
    // ===========================================================

    // ===========================================================
    // Getter & Setter
    // ===========================================================

    // ===========================================================
    // Methods for/from SuperClass/Interfaces
    // ===========================================================

    // ===========================================================
    // Methods
    // ===========================================================
    protected FragmentManager fragmentManager;

    public BaseActivity() {
        this.fragmentManager = getSupportFragmentManager();
    }


    //布局文件ID
    protected abstract int getContentViewId();

    //布局中的FragmentID
    protected abstract int getFragmentContentId();



    protected <T extends View> T $(int id) {
        return (T) super.findViewById(id);
    }

    protected void addFragment(BaseFragment fragment) {
        if (fragment != null) {
            fragmentManager.beginTransaction()
                    .replace(getFragmentContentId(),fragment,fragment.getClass().getSimpleName())
                    .addToBackStack(fragment.getClass().getSimpleName())
                    .commitAllowingStateLoss() ;
        }
    }

    // remove fragment
    protected void removeFragment(){
        if (fragmentManager.getBackStackEntryCount()>1){
            getSupportFragmentManager().popBackStack();
        }else {
            this.finish();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

}
