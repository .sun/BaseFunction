package com.example.baselibrary.activtity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;

import java.util.List;

/**
 * Created by sunsh on 2017/12/18.
 */

public abstract class AppActivity extends BaseActivity {

    protected abstract BaseFragment getFirstFragment();
    //handle Intent

    protected void handleIntent(Intent intent) {

    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(getContentViewId());

        if (null != getIntent()) {
            handleIntent(getIntent());
        }
        List<Fragment> fragmentList = fragmentManager.getFragments();
        if (null == fragmentManager.getFragments() || fragmentList.size() == 0) {
            BaseFragment firstFragment = getFirstFragment();
            if (null != firstFragment) {
                addFragment(firstFragment);
            }
        }

        ActivityManager.getInstance().addActivity(this);
    }


    @Override
    public void onClick(View view) {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ActivityManager.getInstance().finishActivity(this);
    }
}
